package org.contecale.client.gui.model;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.io.File;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;

public class TMFiles implements TableModel {

    private LinkedList<File> listFiles;

    public TMFiles(LinkedList<File> listFiles) {
        this.listFiles = listFiles;
        listFiles.sort(Comparator.comparing(File::getName));
    }

    private String getSize(int size) {
        final long kb = 1024;
        final long mb = 1024*kb;
        final long gb = mb*kb;

        if (size <= kb)
            return new StringBuilder().append(size)
                    .append(" B").toString();
        else if (size <= mb)
            return new StringBuilder().append(size / kb)
                    .append(" KB").toString();
        else if (size <= gb)
            return new StringBuilder().append(size / mb)
                    .append(" MB").toString();
        else
            return new StringBuilder().append(size / gb)
                    .append(" GB").toString();
    }

    private Date getDate(long time) {
        return new Date(time);
    }

    public String getParentPath() {
        return listFiles.isEmpty() ? null : listFiles.get(0).getParent();
    }

    public File getFile(int row) {
        return listFiles.get(row);
    }

    @Override
    public int getRowCount() {
        return listFiles.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0: return "Nombre";
            case 1: return "Tamaño";
            default: return "Modificado";
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return Object.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        File file = listFiles.get(rowIndex);

        switch (columnIndex) {
            case 0: return file.getName();
            case 1: return getSize((int) file.length());
            default: return getDate(file.lastModified())
                    .toString();
        }

    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
    }

}
