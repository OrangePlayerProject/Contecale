package org.contecale.client.gui;

import org.contecale.client.net.Connector;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Session {

    private static final Map<String, Object> mapSession;

    static {
        mapSession = new HashMap<>();
    }

    public static boolean exists(String key) {
        return mapSession.containsKey(key);
    }

    public static Object add(String key, Object value) {
        if (!exists(key)) {
            mapSession.put(key, value);
            return value;
        }
        else
            return null;
    }

    public static Object get(String key) {
        return mapSession.get(key);
    }

    public static void set(String key, Object value) {
        mapSession.put(key, value);
    }

    public static Object remove(String key) {
        return mapSession.remove(key);
    }

}
