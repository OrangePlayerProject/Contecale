package org.contecale.client.gui.info;

public class SessionKeys {
    public static final String CLIENT = "cli";
    public static final String CONNECTOR = "connector";
    public static final String LISTFILES = "lf";
    public static final String FORMMAIN = "fm";
    public static final String FORMMANAGER = "fmgr";
    public static final String REMOTEPARENT = "rp";
    public static final String TMFILES = "tmf";
}
