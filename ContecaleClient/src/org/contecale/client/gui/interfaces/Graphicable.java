package org.contecale.client.gui.interfaces;

import javax.swing.*;

public interface Graphicable {
    public void init();
    public void initComponents();
    public void initOperations();
    public default void loadLookAndFeel() {
        try {
            javax.swing.UIManager.setLookAndFeel(
                    new javax.swing.plaf.metal.MetalLookAndFeel());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }
    public void loadListeners();
}
