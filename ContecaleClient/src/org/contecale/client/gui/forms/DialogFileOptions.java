package org.contecale.client.gui.forms;

import org.contecale.client.gui.Session;
import org.contecale.client.gui.info.SessionKeys;
import org.contecale.client.gui.interfaces.Graphicable;
import org.contecale.client.gui.model.TMFiles;
import org.contecale.client.net.Client;
import org.orangeplayer.common.cmd.Command;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

import static org.contecale.client.gui.info.SessionKeys.FORMMANAGER;

public class DialogFileOptions extends JDialog implements Graphicable {
    private JPanel contentPane;
    private JButton btnOk;
    private JButton btnCancel;
    private JLabel lblOptionsTitle;
    private JButton btnDelete;
    private JPanel panelBotton;
    private JPanel panelTitle;
    private JPanel panelOptions;
    private JPanel panelContent;

    private File selectedFile;

    public DialogFileOptions(File selectedFile) {
        this.selectedFile = selectedFile;
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    @Override
    public void init() {
        initComponents();
        initOperations();
        setVisible(true);
    }

    @Override
    public void initOperations() {

    }

    @Override
    public void initComponents() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btnOk);
        setLocationRelativeTo((Component) Session.get(FORMMANAGER));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        loadListeners();
        initOperations();
    }

    @Override
    public void loadListeners() {
        btnOk.addActionListener(e -> onOK());
        btnCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(),
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        // Buttons
        btnDelete.addActionListener(e -> {
            try {
                Client client = (Client) Session.get(SessionKeys.CLIENT);
                Command cmd = client.deleteFile(selectedFile.getPath());
                dispose();
                JOptionPane.showMessageDialog(this, cmd.getOptions());
                TMFiles newModel = new TMFiles(client.getListFiles(selectedFile.getParent())
                        .getListFiles());
                ((FormManager)Session.get(SessionKeys.FORMMANAGER)).updateTableModel(newModel);

            } catch (IOException | ClassNotFoundException e1) {
                JOptionPane.showMessageDialog(this, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        });

    }
}
