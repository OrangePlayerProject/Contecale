package org.contecale.client.gui.forms;

import org.contecale.client.gui.interfaces.Graphicable;
import org.contecale.client.gui.Session;
import org.contecale.client.gui.model.TMFiles;
import org.contecale.client.net.Client;
import org.orangeplayer.common.cmd.Command;
import org.contecale.common.net.ListPackage;

import javax.swing.*;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumnModel;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import static org.contecale.client.gui.info.SessionKeys.*;

public class FormManager extends JFrame implements Graphicable {

    private JPanel panelManager;
    private JPanel panelMenu;
    private JPanel panelFiles;
    private JTable tblFiles;
    private JButton btnUpload;
    private JLabel lblTitle;
    private JPanel panelOptions;
    private JButton btnValidate;

    public FormManager() {
        Session.set(FORMMANAGER, this);
        TableColumnModel tcm = new DefaultTableColumnModel();
    }

    private void loadFileOptions(File file) {
        new DialogFileOptions(file).init();
    }

    private void loadTableData() throws IOException, ClassNotFoundException {
        TMFiles tmFiles = new TMFiles((LinkedList<File>)Session.get(LISTFILES));
        Session.remove(LISTFILES);
        updateTableModel(tmFiles);
    }

    private JFileChooser getConfiguredFileChooser() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setDialogTitle("Seleccione música a subir");
        return fileChooser;
    }

    private String getCurrentParent(){
        return (String) Session.get(REMOTEPARENT);
    }

    public void updateTableModel(TMFiles model) {
        Session.set(TMFILES, model);
        tblFiles.setModel(model);
        tblFiles.updateUI();
    }

    @Override
    public void init() {
        initComponents();
        initOperations();
        setVisible(true);
    }

    @Override
    public void initOperations() {
        try {
            loadTableData();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initComponents() {
        loadLookAndFeel();
        loadListeners();
        setContentPane(panelManager);
        setLocationRelativeTo((Component) Session.get(FORMMAIN));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
    }

    @Override
    public void loadListeners() {
        tblFiles.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    System.out.println("Dos clicks");
                    TMFiles model = (TMFiles) tblFiles.getModel();
                    int selectedRow = tblFiles.getSelectedRow();
                    if (selectedRow != -1) {
                        File selected = model.getFile(selectedRow);
                        loadFileOptions(selected);
                    }
                }
            }
        });
        btnUpload.addActionListener(e -> {
            JFileChooser fileChooser = getConfiguredFileChooser();
            fileChooser.showOpenDialog(this);

            File[] selectedFiles = fileChooser.getSelectedFiles();

            if (selectedFiles != null) {
                Client client = (Client) Session.get(CLIENT);

                // Por ahora solo usaremos un archivo que no sea directorio
                File selected = selectedFiles[0];
                fileChooser = null;
                if (!selected.isDirectory()) {
                    try {
                        Command response = client.uploadFile(selected, getCurrentParent());
                        ListPackage pack = client.getListFiles(getCurrentParent());
                        Session.set(REMOTEPARENT, pack.getRemoteParent());
                        updateTableModel(new TMFiles(pack.getListFiles()));
                        JOptionPane.showMessageDialog(this, response.getOptions());

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (ClassNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }

            }

        });
        btnValidate.addActionListener(e -> {
            Client client = (Client) Session.get(CLIENT);
            try {
                Command validate = client.validate();
                ListPackage pack = client.getListFiles(getCurrentParent());
                updateTableModel(new TMFiles(pack.getListFiles()));
                JOptionPane.showMessageDialog(this, "Archivos validados correctamente");

            } catch (IOException | ClassNotFoundException e1) {
                JOptionPane.showMessageDialog(this, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

}

