package org.contecale.client.gui.forms;

import org.contecale.client.gui.info.SessionKeys;
import org.contecale.client.gui.interfaces.Graphicable;
import org.contecale.client.gui.Session;
import org.contecale.client.net.Client;
import org.contecale.common.net.ListPackage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import static org.contecale.client.gui.info.SessionKeys.*;

public class FormMain extends JFrame implements Graphicable {
    private JPanel panelMain;
    private JTextField txtIp;
    private JButton btnConnect;

    public FormMain() {
        Session.set(FORMMAIN, this);
    }

    @Override
    public void init() {
        initComponents();
        initOperations();
        this.setVisible(true);
    }

    @Override
    public void initOperations() {

    }

    @Override
    public void initComponents() {
        loadLookAndFeel();
        loadListeners();
        this.setContentPane(panelMain);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.pack();
    }

    @Override
    public void loadListeners() {
        btnConnect.addActionListener(e -> {
            System.out.println("En el action");
            try {
                String host = txtIp.getText().trim();
                if (host.isEmpty()) {
                    txtIp.selectAll();
                    txtIp.requestFocus();
                }
                else {
                    Client client;
                    if (!Session.exists(CLIENT))
                        client = (Client) Session.add(CLIENT, new Client(host));
                    else {
                        client = (Client) Session.remove(CLIENT);
                        client.close();
                        client = new Client(host);
                        Session.add(CLIENT, client);
                    }
                    ListPackage listPackage = client.getListFiles("root");
                    LinkedList<File> listFiles = listPackage.getListFiles();
                    Session.set(LISTFILES, listFiles);
                    Session.set(REMOTEPARENT, listPackage.getRemoteParent());
                    setVisible(false);
                    txtIp.setText(null);
                    System.out.println("Antes de formmanager");
                    new FormManager().init();
                    System.out.println("FormManagerInit");
                }
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(null, e1.getMessage(),
                        "Error", JOptionPane.ERROR_MESSAGE);
                txtIp.requestFocus();
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            }
        });
        txtIp.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    btnConnect.doClick();
            }
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new FormMain().init());
    }
}
