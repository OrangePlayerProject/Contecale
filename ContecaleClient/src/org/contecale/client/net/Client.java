package org.contecale.client.net;

import org.orangeplayer.common.cmd.Command;
import org.contecale.common.cmd.CommandBuilder;
import org.contecale.common.cmd.CommandKey;
import org.contecale.common.net.ListPackage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class Client {
    private Connector connector;

    public Client(String host) throws IOException {
        connector = new Connector(host);
    }

    public Client(Connector connector) {
        this.connector = connector;
    }

    public Connector getConnector() {
        return connector;
    }

    public void close() throws IOException {
        connector.close();
    }

    public CommandBuilder getBuilder() {
        return new CommandBuilder();
    }

    public ListPackage getListFiles(String parent) throws IOException, ClassNotFoundException {
        CommandBuilder builder = getBuilder();
        builder.buildCmd(CommandKey.LIST, parent);
        connector.send(builder.getCmd());
        return (ListPackage) connector.receivObject();
    }

    public Command uploadFile(File file, String remoteParent) throws IOException {
        CommandBuilder builder = getBuilder();
        builder.buildCmd(CommandKey.UPLOAD, remoteParent, file.getName());
        connector.send(builder.getCmd());
        connector.send(Files.readAllBytes(file.toPath()));
        return new Command(connector.receivMsg());
    }

    /*public int uploadManyFiles(File[] files, String remotePath) throws IOException {
        if (files == null)
            return 0;
        int uploadCount = 0;

        CommandBuilder builder = getBuilder();
        builder.buildCmd(CommandKey.UPLOAD);

        File f;

        for (int i = 0; i < files.length; i++) {
            f = files[i];
            if (f.isDirectory()) {
                uploadCount += uploadManyFiles(f.listFiles(), f.getPath());

            }
            else {
                uploadFile(f, remotePath);
                uploadCount++;
            }
        }
        return uploadCount;
    }*/

    public Command deleteFile(String filePath) throws IOException {
        CommandBuilder builder = getBuilder();
        builder.buildCmd(CommandKey.DELETE, filePath);
        connector.send(builder.getCmd());
        return new Command(connector.receivMsg());
    }

    public Command validate() throws IOException {
        CommandBuilder builder = getBuilder();
        builder.buildCmd(CommandKey.VALIDATE);
        connector.send(builder.getCmd());
        return new Command(connector.receivMsg());
    }

}
