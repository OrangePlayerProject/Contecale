package org.contecale.client.net;

import org.contecale.common.io.streams.ContecaleInputStream;
import org.contecale.common.io.streams.ContecaleOutputStream;
import org.contecale.common.sys.ContecaleInfo;
import org.contecale.common.sys.ServerProperties;
import org.orangeplayer.common.cmd.Command;
import org.orangeplayer.common.interfaces.Communicable;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Connector implements Communicable {

    private Socket sock;
    private ContecaleOutputStream outputStream;
    private ContecaleInputStream inputStream;

    public Connector(String host) throws IOException {
        this(new Socket(host, Integer.parseInt(
                new ContecaleInfo().getProperty(ServerProperties.DEFAULTPORT))));
    }

    public Connector(Socket sock) throws IOException {
        this(sock, sock.getOutputStream(), sock.getInputStream());
    }

    public Connector(Socket sock, ContecaleOutputStream outputStream,
                     ContecaleInputStream inputStream) {
        this.sock = sock;
        this.outputStream = outputStream;
        this.inputStream = inputStream;
    }

    public Connector(Socket sock, OutputStream outputStream, InputStream inputStream) {
        this(sock, new ContecaleOutputStream(outputStream),
                new ContecaleInputStream(inputStream));
    }

    public void close() throws IOException {
        outputStream.close();
        inputStream.close();
        sock.close();
    }

    @Override
    public void send(byte[] data) throws IOException {
        outputStream.writeBytes(data);
    }

    @Override
    public void send(String str) throws IOException {
        outputStream.writeString(str);
    }

    public void send(Command cmd) throws IOException {
        send(cmd.toString());
    }

    @Override
    public void send(int data) throws IOException {
        outputStream.writeInt(data);
    }

    public void send(byte data) throws IOException {
        outputStream.writeByte(data);
    }

    @Override
    public void send(Object obj) throws IOException {
        outputStream.writeObject(obj);
    }

    @Override
    public byte[] receiv() throws IOException {
        return inputStream.readBytes();
    }

    @Override
    public String receivMsg() throws IOException {
        return inputStream.readString();
    }

    @Override
    public byte receivByte() throws IOException {
        return inputStream.readByte();
    }

    @Override
    public int receivInt() throws IOException {
        return inputStream.readInt();
    }

    @Override
    public Object receivObject() throws IOException, ClassNotFoundException {
        return inputStream.readObject();
    }

}
