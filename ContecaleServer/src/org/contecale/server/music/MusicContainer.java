package org.contecale.server.music;

import org.contecale.common.io.FileData;
import org.contecale.common.sys.ServerProperties;
import org.contecale.server.sys.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;

public class MusicContainer {
    private ServerInfo info;
    private File musicFolder;
    //private LinkedList<String> listSoundsPath;

    public MusicContainer() {
        info = new ServerInfo();
        musicFolder = new File(info.getProperty(ServerProperties.CONTAINERPATH));
    }

    private boolean isValidSong(File song) {
        return MusicAnalyzer.isValidSong(song);
    }

    public String getRemoteParent() {
        return info.getProperty(ServerProperties.CONTAINERPATH);
    }

    // CRUD de musica

    public boolean addSound(FileData fileData) throws IOException {
        File newFile = new File(fileData.getRemoteParent(), fileData.getName());

        // Podria crearse un secundario para analizar los bytes aparte
        // y asi no dañar uno valido
        System.out.println("Existe antes: "+newFile.exists());
        System.out.println("newFilePath: "+newFile.getPath());
        // Buscar la forma con un ByteArrayInputStream de validar los bytes
        // antes de crear el archivo
        Files.write(newFile.toPath(), fileData.getData(), StandardOpenOption.TRUNCATE_EXISTING);
        if (!MusicAnalyzer.isValidSong(newFile)) {
            Logger.getLogger(getClass(), "RutaNovalida", newFile.getPath()).printMsg();
            System.out.println("No es valido");
            newFile.delete();
            return false;
        }

        newFile.setLastModified(fileData.getFileInfo().getModified());
        System.out.println("Existe nuevo archivo: "+newFile.exists());
        return true;
    }

    public boolean createFolder(String parent, String name) {
        File folder = new File(parent, name);
        if (folder.exists())
            return false;
        return folder.mkdir();
    }

    public boolean deleteFile(File file) {
        if (!file.exists())
            return false;
        if (file.isDirectory()) {
            File[] dirFiles = file.listFiles();

            if (dirFiles != null) {
                for (int i = 0; i < dirFiles.length; i++)
                    deleteFile(dirFiles[i]);
                return true;
            }
            else
                return false;
        }
        else
            return file.delete();
    }

    public boolean deleteFile(String path) {
        return deleteFile(new File(path));
    }

    public File getMusicFolder() {
        return musicFolder;
    }

    public LinkedList<File> getListFiles(String path) {
        LinkedList<File> listFiles = new LinkedList<>();
        if (path.equals("root"))
            path = musicFolder.getPath();

        File[] fldFiles = new File(path).listFiles();
        if (fldFiles != null)
            for (int i = 0; i < fldFiles.length; i++)
                listFiles.add(fldFiles[i]);
        return listFiles;
    }

    public void validateFiles(File folder) {
        File[] fldFiles = folder.listFiles();
        if (fldFiles != null) {
            File file;
            for (int i = 0; i < fldFiles.length; i++) {
                file = fldFiles[i];
                if (file.isDirectory())
                    validateFiles(file);
                else if (!isValidSong(file)) {
                    file.delete();
                }
            }
        }
    }

    public void validateFiles() {
        validateFiles(musicFolder);
    }

}
