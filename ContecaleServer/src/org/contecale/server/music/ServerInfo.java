package org.contecale.server.music;

import org.contecale.common.sys.ServerProperties;
import org.orangeplayer.common.sys.PropertiesManager;
import org.orangeplayer.common.sys.SysInfo;

import java.io.File;

public class ServerInfo extends PropertiesManager {

    public ServerInfo() {
        super("infoServer.properties", "ServerInfo");
    }

    public ServerInfo(File infoFile) {
        super(infoFile, "ServerInfo");
    }

    public ServerInfo(String infoFilePath) {
        this(new File(infoFilePath));
    }

    @Override
    protected void saveDefaultData() {
        addProperty(ServerProperties.CONTAINERPATH, SysInfo.PARENTFOLDER.getPath());
        addProperty(ServerProperties.DEFAULTPORT,
                String.valueOf(3200));
    }

    public int getPropertyAsInteger(String key) {
        return Integer.parseInt(props.getProperty(key));
    }

    // Propiedades

}
