package org.contecale.server.net;

import org.orangeplayer.common.cmd.Command;
import org.contecale.common.cmd.CommandInfo;
import org.contecale.common.cmd.CommandKey;
import org.contecale.common.io.FileData;
import org.contecale.common.io.FileInfo;
import org.contecale.common.io.streams.ContecaleInputStream;
import org.contecale.common.io.streams.ContecaleOutputStream;
import org.contecale.common.net.ListPackage;
import org.contecale.common.sys.ResponseType;
import org.contecale.server.music.MusicContainer;
import org.contecale.server.music.ServerInfo;
import org.contecale.server.sys.Logger;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;

public class TClient extends Thread {
    private Socket cliSock;
    private ContecaleOutputStream outputStream;
    private ContecaleInputStream inputStream;

    private ServerInfo serverInfo;
    private MusicContainer container;
    private CommandInterpreter interpreter;
    private CommandInfo cmdInfo;

    private boolean isConnected;

    private StringBuilder sbResponses;

    public TClient(Socket cliSock) throws IOException {
        this.cliSock = cliSock;
        outputStream = new ContecaleOutputStream(cliSock.getOutputStream());
        inputStream = new ContecaleInputStream(cliSock.getInputStream());
        container = new MusicContainer();
        cmdInfo = new CommandInfo();
        sbResponses = new StringBuilder();
        isConnected = false;
        instanceInterpreter();
    }

    // lo que se hizo con filedata en los streams creados se hara en este hilo

    private void instanceInterpreter() {
        interpreter = cmd -> {
            final String order = cmd.getOrder().trim();
            if (order.equals(cmdInfo.getProperty(CommandKey.DELETE))) {
                final String remotePath = cmd.getOptionAt(0);
                // Se puede borrar archivo o carpeta
                if (container.deleteFile(remotePath))
                    buildResponse(ResponseType.OK, "Archivo eliminado correctamente");
                else
                    // Se supone que no existe xd
                    buildResponse(ResponseType.ERR, "Archivo no existe");
            }
            else if (order.equals(cmdInfo.getProperty(CommandKey.LIST))) {
                String firstOption = cmd.getOptionAt(0);
                String remoteParent = firstOption.equals("root") ? container.getRemoteParent()
                        : firstOption;
                LinkedList<File> listFiles =
                        container.getListFiles(remoteParent);
                outputStream.writeObject(new ListPackage(remoteParent, listFiles));
            }
            else if (order.equals(cmdInfo.getProperty(CommandKey.UPLOAD))) {
                // Ver si mas adelante se leen los datos del filedata como json
                FileData fileData = new FileData();
                fileData.setFileInfo(new FileInfo());
                FileInfo info = fileData.getFileInfo();
                info.setRemoteParent(cmd.getOptionAt(0));
                info.setName(cmd.getOptionAt(1));
                fileData.setData(inputStream.readBytes());
                System.out.println("Bytes data: "+fileData.getData().length);
                boolean ok = container.addSound(fileData);
                if (ok)
                    buildResponse(ResponseType.OK, "Cancion subida correctamente");
                else
                    buildResponse(ResponseType.ERR, "Formato de archivo no soportado");
            }
            else if (order.equals(cmdInfo.getProperty(CommandKey.VALIDATE))) {
                container.validateFiles();
                buildResponse(ResponseType.OK, "Archivos validados");
            }
            else {
                // Comando desconocido
                System.out.println("Comando desconocido");
                buildResponse(ResponseType.ERR, "Comando Desconocido");
            }
        };
    }

    private void waitForData() throws IOException {
        while (inputStream.available() == 0);
    }

    private void buildResponse(String responseType, String msg) {
        sbResponses.append(responseType).append(' ').append(msg);
    }

    public boolean isConnected() {
        return isConnected;
    }

    public String getReceivData() throws IOException {
        return inputStream.readString();
    }

    public void sendResponse() throws IOException {
        if (sbResponses.length() > 0) {
            String strResponse = sbResponses.toString();
            Logger.getLogger(getClass(), "strResponse", strResponse).printMsg();
            outputStream.writeString(strResponse);
            sbResponses.delete(0, sbResponses.length());
        }
    }

    public void disconnect(){
        isConnected = false;
    }

    @Override
    public void run() {
        System.out.println("TClient:run");
        isConnected = true;
        while (isConnected) {
            try {
                interpreter.interprate(new Command(getReceivData()));
                sendResponse();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            outputStream.close();
            inputStream.close();
            cliSock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
