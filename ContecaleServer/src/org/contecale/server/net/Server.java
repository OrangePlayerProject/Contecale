package org.contecale.server.net;

import org.contecale.common.sys.ServerProperties;
import org.contecale.server.music.ServerInfo;
import sun.rmi.runtime.Log;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {
    private ServerSocket serverSocket;
    private LinkedList<TClient> listClients;
    private ClientListener cliListener;
    private ServerInfo serverInfo;
    private boolean isConnected;

    private static Server server;


    public static void createInstance() throws IOException {
        server = new Server();
    }

    public static Server getInstance() throws IOException {
        return server;
    }

    private Server() throws IOException {
        this.serverSocket = new ServerSocket(
                new ServerInfo()
                        .getPropertyAsInteger(ServerProperties.DEFAULTPORT));
        listClients = new LinkedList<>();
        isConnected = false;
        configListener();
    }

    private void configListener() {
        cliListener = socket -> {
            new Thread(()->{
                TClient client = null;
                try {
                    client = new TClient(socket);
                    addClient(client);
                    System.out.println("Client added!");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        };
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void disconnect() {
        isConnected = false;
    }

    public void addClient(TClient client) {
        if (!client.isAlive())
            client.start();
        listClients.add(client);
    }

    public void start() throws IOException {
        isConnected = true;
        System.out.println("Server Started");
        while (isConnected) {
            cliListener.onClientConnected(serverSocket.accept());
        }
        listClients.parallelStream().forEach(cli->{
            cli.disconnect();
        });
        serverSocket.close();
    }

    public static void main(String[] args) throws IOException {
        Server.createInstance();
        Server.getInstance().start();
    }


}
