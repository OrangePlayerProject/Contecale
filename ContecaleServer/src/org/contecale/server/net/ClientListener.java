package org.contecale.server.net;

import java.net.Socket;

@FunctionalInterface
public interface ClientListener {
    public void onClientConnected(Socket socket);
}
