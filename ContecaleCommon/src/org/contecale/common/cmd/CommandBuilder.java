package org.contecale.common.cmd;

import org.orangeplayer.common.cmd.Command;

public class CommandBuilder {
    private Command cmd;
    private CommandInfo cmdInfo;

    public CommandBuilder() {
        cmdInfo = new CommandInfo();
    }

    public void buildCmd(String orderKey, String... options) {
        cmd = new Command(cmdInfo.getProperty(orderKey));
        cmd.addOptions(options);
    }

    public void buildCmd(String orderKey) {
        buildCmd(orderKey, null);
    }

    public void clearCmd() {
        cmd = new Command();
    }

    public void clearCmdOptions() {
        cmd.removeOptions();
    }

    public void addOptions(String... options) {
        cmd.addOptions(options);
    }

    public Command getCmd() {
        return cmd;
    }

}
