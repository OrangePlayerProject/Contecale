/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.contecale.common.io.streams;

import org.contecale.common.io.FileData;
import org.contecale.common.io.FileInfo;
import org.orangeplayer.common.streams.OrangeInputStream;

import java.io.IOException;
import java.io.InputStream;


/**
 *
 * @author martin
 */
public class ContecaleInputStream extends OrangeInputStream {

    public ContecaleInputStream(InputStream socketStream) {
        super(socketStream);
    }

    private FileInfo readFileInfo() throws IOException {
        FileInfo info = new FileInfo();
        info.setRemoteParent(readString());
        info.setName(readString());
        info.setLenght(readLong());
        info.setModified(readLong());
        info.setDirectory(readBoolean());
        return info;
    }

    public FileData readFileData() throws IOException {
        FileData fileData = new FileData();
        fileData.setFileInfo(readFileInfo());
        fileData.setData(readBytes());
        return fileData;
    }

}
