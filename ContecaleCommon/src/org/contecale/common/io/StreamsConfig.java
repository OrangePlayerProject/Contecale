package org.contecale.common.io;

public class StreamsConfig {
    //public static final int SENDBUFFSIZE = 106464;
    //public static final int SENDBUFFSIZE = 319392;
    public static final int SENDBUFFSIZE = 1024*(512+(32*5));
    public static final int RECVBUFFSIZE = SENDBUFFSIZE;
}
