package org.contecale.common.io;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;

public class FileData implements Serializable {

    private FileInfo fileInfo;
    private byte[] data;

    public FileData() {}

    public FileData(File file) throws IOException {
        fileInfo = new FileInfo();
        fileInfo.setDirectory(file.isDirectory());
        fileInfo.setModified(file.lastModified());
        fileInfo.setLenght(file.length());
        fileInfo.setName(file.getName());
        fileInfo.setRemoteParent(file.getParent());
        data = Files.readAllBytes(file.toPath());
    }

    public FileData(FileInfo fileInfo, byte[] data) {
        this.fileInfo = fileInfo;
        this.data = data;
    }

    public FileInfo getFileInfo() {
        return fileInfo;
    }

    public void setFileInfo(FileInfo fileInfo) {
        this.fileInfo = fileInfo;
    }

    public String getRemoteParent() {
        return fileInfo.getRemoteParent();
    }

    public String getName() {
        return fileInfo.getName();
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

}
