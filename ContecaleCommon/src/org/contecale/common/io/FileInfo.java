package org.contecale.common.io;

import java.io.File;

public class FileInfo {
    private String remoteParent;
    private String name;
    private long lenght;
    private long modified;
    private boolean isDirectory;

    public FileInfo() {

    }

    public FileInfo(File file) {
        remoteParent = file.getParent();
        name = file.getName();
        lenght = file.length();
        modified = file.lastModified();
        isDirectory = file.isDirectory();
    }

    public FileInfo(String remoteParent,
                    String name, long lenght, long modified, boolean isDirectory) {
        this.remoteParent = remoteParent;
        this.name = name;
        this.lenght = lenght;
        this.modified = modified;
        this.isDirectory = isDirectory;
    }

    public String getRemoteParent() {
        return remoteParent;
    }

    public void setRemoteParent(String remoteParent) {
        this.remoteParent = remoteParent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getLenght() {
        return lenght;
    }

    public void setLenght(long lenght) {
        this.lenght = lenght;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public void setDirectory(boolean directory) {
        isDirectory = directory;
    }
}
