package org.contecale.common.sys;

import org.orangeplayer.common.sys.PropertiesManager;
import org.orangeplayer.common.sys.SysInfo;

import java.io.File;

public class ContecaleInfo extends PropertiesManager {

    public ContecaleInfo() {
        this(SysInfo.PARENTFOLDER.getPath()+"/contecaleInfo.properties", "Contecale Info");
    }

    public ContecaleInfo(File infoFile, String propertiesTitle) {
        super(infoFile, propertiesTitle);
    }

    public ContecaleInfo(String infoFilePath, String propertiesTitle) {
        super(infoFilePath, propertiesTitle);
    }

    @Override
    protected void saveDefaultData() {
        addProperty(ServerProperties.DEFAULTPORT,
                String.valueOf(3200));
    }

}
