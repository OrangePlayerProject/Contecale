package org.contecale.common.ontesting;

import org.contecale.common.io.FileData;
import org.contecale.common.io.FileInfo;
import org.contecale.common.io.StreamsConfig;
import org.contecale.common.io.streams.ContecaleInputStream;
import org.contecale.common.io.streams.ContecaleOutputStream;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import static org.contecale.common.io.StreamsConfig.SENDBUFFSIZE;

public class StreamsDefaultTest {
    public static void main(String[] args) throws IOException {
        int port = 4000;
        ServerSocket serverSocket = new ServerSocket(port);
        Socket client = new Socket("localhost", port);
        Socket serverClient = serverSocket.accept();

        ContecaleOutputStream outputStream = new ContecaleOutputStream(
                client.getOutputStream());
        ContecaleInputStream inputStream = new ContecaleInputStream(
                serverClient.getInputStream());

        client.setSendBufferSize(SENDBUFFSIZE);
        serverClient.setReceiveBufferSize(StreamsConfig.RECVBUFFSIZE);

        int buffSize = SENDBUFFSIZE*4+1;
        int sendBuffmb = buffSize/1024/1024;
        System.out.println("MBTransfered: "+sendBuffmb);

        FileData fileData = new FileData();
        fileData.setFileInfo(new FileInfo("parent", "name", 0, 0, false));
        byte[] sendBuffer = new byte[buffSize];
        final Random random = new Random();

        for (int i = 0; i < buffSize; i++)
            sendBuffer[i] = (byte) random.nextInt();
        fileData.setData(sendBuffer);

        new Thread(() -> {
            byte[] recv = new byte[0];
            try {
                while (inputStream.available() == 0);
                long ti, tf;
                ti = System.currentTimeMillis();
                FileData data = inputStream.readFileData();
                tf = System.currentTimeMillis();
                tf = (tf/1000-ti/1000);
                System.out.println(data.getRemoteParent());
                System.out.println(data.getName());
                System.out.println(data.getFileInfo().getLenght());
                System.out.println(data.getFileInfo().getModified());
                System.out.println(data.getFileInfo().isDirectory());
                System.out.println(data.getData().length);
                String speed = new DecimalFormat("#0.00")
                        .format(((double)sendBuffmb)/tf);
                System.out.println("RecvSpeed: "+speed);
                recv = data.getData();

            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("IsEquals: "+isEquals(sendBuffer, recv));
        }).start();

        long ti, tf;
        ti = System.currentTimeMillis();
        outputStream.writeFileData(fileData);
        tf = System.currentTimeMillis();
        tf = (tf/1000-ti/1000);
        String speed = new DecimalFormat("#0.00").format(((double)sendBuffmb)/tf);
        System.out.println("SendSpeed: "+speed);

    }

    public static boolean isEquals(byte[] a, byte[] b) {
        if (a.length != b.length)
            return false;

        boolean equals = true;
        for (int i = 0; i < a.length; i++)
            if (a[i] != b[i]) {
                System.out.println(a[i]+"-"+b[i]);
                equals = false;
                break;
            }
        return equals;
    }

}
