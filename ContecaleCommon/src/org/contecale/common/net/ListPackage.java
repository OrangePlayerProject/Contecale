package org.contecale.common.net;

import java.io.File;
import java.io.Serializable;
import java.util.LinkedList;

public class ListPackage implements Serializable {
    private String remoteParent;
    private LinkedList<File> listFiles;

    public ListPackage(String remoteParent, LinkedList<File> listFiles) {
        this.remoteParent = remoteParent;
        this.listFiles = listFiles;
    }

    public String getRemoteParent() {
        return remoteParent;
    }

    public void setRemoteParent(String remoteParent) {
        this.remoteParent = remoteParent;
    }

    public LinkedList<File> getListFiles() {
        return listFiles;
    }

    public void setListFiles(LinkedList<File> listFiles) {
        this.listFiles = listFiles;
    }

}
