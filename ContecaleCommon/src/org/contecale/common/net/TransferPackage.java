package org.contecale.common.net;

import java.io.Serializable;

public class TransferPackage<T> implements Serializable {
    private final T object;

    public TransferPackage(T object) {
        this.object = object;
    }

    public T getObject() {
        return object;
    }

}
