## 0.1 Beta
	1.- Creacion de arquitectura de ContecaleCommon y ContecaleServer
	2.- Creacion de protocolos de transmision en el nivel de aplicacion

## 0.2 Beta
	1.- Outputstream e Inputstream funcionando

## 0.2.1 Beta
	1.- Buscando el modo mas optimo de enviar datos

## 0.2.2 Beta
	1.- Streams corregidos
	2.- Se crean metodos writeBytes y writeObject personalizados
	3.- Se crean metodos readBytes y readObject personalizados
## 0.2.3 Beta
	1.- Servidor creado, faltan hacer pruebas

## 0.3 Beta
	1.- Servidor terminado, falta testear y pulir
	2.- Se construye cliente y se logra conectar a servidor
	3.- Crear operacion de subida de archivos (Puede ser enviando objetos filedata)
## 0.3.1 Beta
	1.- Se agrega un sleep al inputstream para que el servidor aliviane la carga de la cpu
	2.- Se agregan elementos graficos para las operaciones en el cliente
	3.- Se modofica arquitectura de formularios para una mayor optimizacion
	4.- Se agrega ContecaleClient al repositorio
## 0.3.2 Beta
	1.- Se crea clase Logger para manejar mensajes del servidor
	2.- Se corrige error al validar archivos en el momento de subir
	3.- Se corrige error de deteccion de carpeta padre remota

## 0.3.3 Beta
	1.- Se traslada clase Logger a libreria externa orangecommon

